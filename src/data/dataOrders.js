const dataOrders = [
  {
    numberOrder: 210,
    id: '#004127',
    dateOrder: 'July 28, 2023 10:17 PM',
    price: 125,
    statusOrder: 'New',
  },
  {
    numberOrder: 211,
    id: '#004126',
    dateOrder: 'July 29, 2023 12:01 PM',
    price: 200,
    statusOrder: 'New',
  },
  {
    numberOrder: 212,
    id: '#004125',
    dateOrder: 'July 29, 2023 7:17 PM',
    price: 1050,
    statusOrder: 'On hold',
  },
  {
    numberOrder: 213,
    id: '#004124',
    dateOrder: 'July 29, 2023 9:27 PM',
    price: 78,
    statusOrder: 'Delivery',
  },
  {
    numberOrder: 214,
    id: '#004123',
    dateOrder: 'July 30, 2023 7:33 PM',
    price: 2280,
    statusOrder: 'New',
  },
]

export { dataOrders };