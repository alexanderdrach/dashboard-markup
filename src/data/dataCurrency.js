const dataCurrency = [
  {
    id: 'cyrrency-1',
    name: 'USD-EUR',
    desc: 'Dollars to Euros',
    value: 0.89102,
  },
  {
    id: 'cyrrency-2',
    name: 'USD-СNY',
    desc: 'Dollars to Yuan',
    value: 7.219,
  },
  {
    id: 'cyrrency-3',
    name: 'USD-GPB',
    desc: 'Dollars to Pounds',
    value: 0.77321,
  },
  {
    id: 'cyrrency-4',
    name: 'USD-AUD',
    desc: 'Dollars to Australian Dollars',
    value: 1.47721,
  },
  {
    id: 'cyrrency-5',
    name: 'USD-JPY',
    desc: 'Dollars to Yen',
    value: 139.7527,
  },
]

export { dataCurrency };