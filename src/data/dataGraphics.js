const dataTotalRevenue = [
  {
    name: 'Page A',
    uv: 4000,
    pv: 8000,
    amt: 2400,
  },
  {
    name: 'Page B',
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: 'Page C',
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: 'Page D',
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: 'Page E',
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: 'Page F',
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: 'Page G',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

const dataTotalOrders = [
  {
    name: 'Page A',
    uv: 4000,
    pv: 1000,
    amt: 2400,
  },
  {
    name: 'Page B',
    uv: 3000,
    pv: 3398,
    amt: 2210,
  },
  {
    name: 'Page C',
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: 'Page D',
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: 'Page E',
    uv: 1890,
    pv: 800,
    amt: 2181,
  },
  {
    name: 'Page F',
    uv: 2390,
    pv: 2000,
    amt: 2500,
  },
  {
    name: 'Page G',
    uv: 3490,
    pv: 900,
    amt: 2100,
  },
];

const dataAbandonedCarts = [
  {
    name: 'Page A',
    uv: 4000,
    pv: 1000,
    amt: 2400,
  },
  {
    name: 'Page B',
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: 'Page C',
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: 'Page D',
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: 'Page E',
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: 'Page F',
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: 'Page G',
    uv: 3490,
    pv: 4300,
    amt: 2100,
  },
];

const dataTotalVisitors = [
  {
    name: 'July 24',
    uv: 400,
    pv: 500,
    amt: 2400,
  },
  {
    name: 'July 25',
    uv: 400,
    pv: 1400,
    amt: 2210,
  },
  {
    name: 'July 26',
    uv: 300,
    pv: 500,
    amt: 2290,
  },
  {
    name: 'July 27',
    uv: 400,
    pv: 700,
    amt: 2000,
  },
  {
    name: 'July 28',
    uv: 350,
    pv: 800,
    amt: 2181,
  },
  {
    name: 'July 29',
    uv: 600,
    pv: 1700,
    amt: 2500,
  },
  {
    name: 'July 30',
    uv: 350,
    pv: 1550,
    amt: 2100,
  },
];

const dataTraffic = [
  {
    id: 'traffic-1',
    trafficName: 'Google',
    trafficValue: 3902,
    backgroundLine: '#72DCDA',
    backgroungValue: '#3EBCBA',
    trafficWidth: 39,
  },
  {
    id: 'traffic-2',
    trafficName: 'Direct entries',
    trafficValue: 2856,
    backgroundLine: '#BDCCFF',
    backgroungValue: '#85A0FF',
    trafficWidth: 28,
  },
  {
    id: 'traffic-3',
    trafficName: 'Social media',
    trafficValue: 1612,
    backgroundLine: '#FFB3A3',
    backgroungValue: '#FD7C60',
    trafficWidth: 16,
  },
  {
    id: 'traffic-4',
    trafficName: 'Others',
    trafficValue: 754,
    backgroundLine: '#DDE3F2',
    backgroungValue: '#CCD2E3',
    trafficWidth: 8,
  },
]

export { dataTotalRevenue, dataTotalOrders, dataAbandonedCarts, dataTotalVisitors, dataTraffic };