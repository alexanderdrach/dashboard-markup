function OrderRow({item}) {
  let classStatus = '';
  switch(item.statusOrder) {
    case 'New':
      classStatus = 'orders__status_new';
      break;
      case 'Delivery':
      classStatus = 'orders__status_delivery';
      break;
      case 'On hold':
      classStatus = 'orders__status_on-hold';
      break;
  }
  return (
    <div className="orders__row">
      <div className="orders__col orders__number">{item.numberOrder}</div>
      <div className="orders__col orders__id">{item.id}</div>
      <div className="orders__col orders__date">{item.dateOrder}</div>
      <div className="orders__col orders__price">$<span>{item.price}</span></div>
      <div className={`orders__status ${classStatus}`}>{item.statusOrder}</div>
    </div>
  )
}

export default OrderRow;