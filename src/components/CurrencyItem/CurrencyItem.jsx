function CurrencyItem({item}) {
  return (
    <div className="currency__item">
     {item.icon && <img className="currency__icon" src={item.icon} alt=""/>}
      <div className="currency__title">
        <div className="currency__name">{item.name}</div>
        <div className="currency__desc">{item.desc}</div>
      </div>
      <div className="currency__value">{item.type === 'crypto' ? '$' : null}<span>{item.value}</span></div>
    </div>
  )
}

export default CurrencyItem;