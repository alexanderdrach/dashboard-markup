import { Link, NavLink } from "react-router-dom";

function Sidebar() {
  return (
    <div className="sidebar">

      <div className="logo">
        <Link to="/">
          <div className="logo__icon">
            <img src="/images/logo-icon.svg" alt="logo" />
          </div>
          <div className="logo__text">
            <img src="/images/logo-text.svg" alt="Swiftdex" />
          </div>
        </Link>
      </div>
      <nav className="nav">
        <NavLink className="nav__item nav__item_dashboard" to="/">Dashboard</NavLink>
        <NavLink className="nav__item nav__item_orders" to="/orders">Orders</NavLink>
        <NavLink className="nav__item nav__item_products" to="products">Products</NavLink>
        <NavLink className="nav__item nav__item_team" to="/team">Team</NavLink>
        <NavLink className="nav__item nav__item_settings" to="/settings">Settings</NavLink>
      </nav>
      <button className="nav__item sidebar__control">Minimize</button>
    </div>
  )
}

export default Sidebar;