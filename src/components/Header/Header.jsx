function Header({titlePage}) {
  return (
    <header className="page__header">
      <h1 className="page__title">{titlePage}</h1>

      <div className="header-nav">
        <button className="btn-icon btn-calendar" type="button">Last week</button>
        <button className="btn-icon btn-notification has-notification" type="button"></button>
        <button className="btn-icon btn-chat has-message" type="button"></button>
        <div className="user">
          <div className="user__icon">
            <img src="/images/user.jpg" alt="user" />
          </div>
          <div className="user__name">
            Stephen Powder
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header;