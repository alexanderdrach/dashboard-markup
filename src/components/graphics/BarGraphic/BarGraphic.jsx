import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';


function BarGraphic({data, title, value, gridCol, classModificator}) {
  return (
    <div className={`block-item ${gridCol} ${classModificator}`}>
      <div className="block-item__header">
        <h3 className="block-item__title">
          {title}
        </h3>
        <div className="block-item__value">{value}</div>
        <div className="maked-orders">Maked orders</div>
      </div>
      <div className="block-item__content">
      <ResponsiveContainer width="100%" height={160}>
        <BarChart
          width={500}
          height={300}
          data={data}
          margin={{
            top: 10,
            right: 0,
            left: 0,
            bottom: 0,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" tick={{ fontSize: '14px', fill: '#797D95', color: '#797D95', fontFamily: 'Onest' }}/>
          <YAxis type="number" tick={{ fontSize: '14px', fill: '#797D95', color: '#797D95', fontFamily: 'Onest' }} domain={[0, 2000]} tickCount={5} interval={0}/>
          <Bar dataKey="pv" fill="#C6C6C6" minPointSize={5} barSize={10} radius={[4, 4, 4, 4]}/>
          <Bar dataKey="uv" fill="#85A0FF" minPointSize={10} barSize={10} radius={[4, 4, 4, 4]}/>
        </BarChart>
      </ResponsiveContainer>
      </div>
    </div>
  )
}

export default BarGraphic;