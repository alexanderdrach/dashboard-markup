import { LineChart, Line, XAxis, YAxis, CartesianGrid, Legend, ResponsiveContainer, PieChart, Pie, Cell } from 'recharts';

function LineGraphic({data, height, color, title, value, percent, classPercent, gridCol, classModificator}) {
  return (
    <div className={`block-item ${gridCol} ${classModificator}`}>
      <div className="block-item__header">
        <h3 className="block-item__title">{title}</h3>
        <div className={`block-item__percent ${classPercent}`}><span>{percent}</span>%</div>
      </div>
      <div className="block-item__content">
        <div className="block-item__value">$<span>{value}</span></div>
        <ResponsiveContainer width="60%" height={height}>
          <LineChart width={300} height={900} data={data}>
            <Line type="monotone" dataKey="pv" stroke={color} strokeWidth={2} />
          </LineChart>
        </ResponsiveContainer>
      </div>
    </div>
  )
}

export default LineGraphic;