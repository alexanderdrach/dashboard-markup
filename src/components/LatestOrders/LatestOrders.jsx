import OrderRow from '../OrderRow';

function LatestOrders({ dataOrders, title, gridCol, classModificator }) {
  return (
    <div className={`block-item ${gridCol} ${classModificator}`}>
      <div className="block-item__header">
        <h3 className="block-item__title">{title}</h3>
        <a className="view-all" href="#">View all</a>
      </div>
      <div className="block-item__content">
        <div className="orders">
          <div className="orders__header">
            <div className="orders__row">
              <div className="orders__col orders__number">No</div>
              <div className="orders__col orders__id">ID</div>
              <div className="orders__col orders__date">Date</div>
              <div className="orders__col orders__price">Price</div>
              <div className="orders__status">Status</div>
            </div>
          </div>
          <div className="order-list">{dataOrders.map(item => {
            return (
              <OrderRow key={item.id} item={item} />
            )
          })}
          </div>
        </div>
      </div>
    </div>
  )
}

export default LatestOrders;