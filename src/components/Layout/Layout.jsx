import { Outlet } from "react-router-dom";
import Sidebar from "../../components/Sidebar";

function Layout() {
  return (
    <div className="page">
      <div className="page__inner">
        <Sidebar />
        <div className="page__content">
          <main>
            <Outlet />
          </main>
        </div>
      </div>
    </div>
  )
}


export default Layout;