import CurrencyItem from "../CurrencyItem";

function CurrencyList ({data}) {
  return (
    <div className="currency__list">
    {data.map(item => {
      return (
        <CurrencyItem item={item} key={item.id}/>
      )
    })}
    </div>
  )
}

export default CurrencyList;