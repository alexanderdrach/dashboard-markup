window.onload = function () {
  const sidebarControl = document.querySelector('.sidebar__control');
  const page = document.querySelector('.page');

  sidebarControl.addEventListener('click', () => {
    page.classList.toggle('sidebare-minimize');
  });
}
