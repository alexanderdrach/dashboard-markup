import "./index.scss";
import "./js/sidebar";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Layout from "./components/Layout";
import HomePage from "./pages/HomePage";
import OrdersPage from "./pages/OrdersPage";
import ProductsPage from "./pages/ProductsPage";
import SettingsPage from "./pages/SettingsPage";
import TeamPage from "./pages/TeamPage";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
      <Routes>
        <Route path="/" element={<Layout/>}>
          <Route index element={<HomePage titlePage={'Welcome back, Stephen!'}/>}/>
          <Route path="orders" element={<OrdersPage titlePage={'Orders'}/>}/>
          <Route path="products" element={<ProductsPage titlePage={'Products'}/>}/>
          <Route path="settings" element={<SettingsPage titlePage={'Settings'}/>}/>
          <Route path="team" element={<TeamPage titlePage={'Team'}/>}/>
        </Route>
      </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
