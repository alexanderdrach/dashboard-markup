import { dataTotalRevenue, dataTotalOrders, dataAbandonedCarts, dataTotalVisitors, dataTraffic } from '../data/dataGraphics';
import { dataOrders } from "../data/dataOrders";
import { dataCurrency } from "../data/dataCurrency";
import { dataCrypto } from "../data/dataCrypto";

import Header from "../components/Header";
import LineGraphic from "../components/graphics/LineGraphic";
import BarGraphic from "../components/graphics/BarGraphic";
import HorizontalGraphic from '../components/graphics/HorizontalGraphic';
import LatestOrders from "../components/LatestOrders";
import Filter from "../components/Filter";
import CurrencyList from "../components/CurrencyList";

function HomePage({ titlePage }) {
  return (
    <>
      <Header titlePage={titlePage} />
      <div className="wrapper-blocks">
        <div className="data-blocks">
          <div className="blocks-list">
            <LineGraphic
              data={dataTotalRevenue}
              height={70}
              color='#3EBCBA'
              title='Total revenue'
              value={12246}
              percent={2.2}
              classPercent='block-item__percent_positive'
              gridCol='col-1'
              classModificator='block-item_total-revenue'
            />
            <LineGraphic
              data={dataTotalOrders}
              height={70}
              color='#85A0FF'
              title='Total orders'
              value={1846}
              percent={1.2}
              classPercent='block-item__percent_negative'
              gridCol='col-1'
              classModificator='block-item_total-orders'
            />
            <LineGraphic
              data={dataAbandonedCarts}
              height={70}
              color='#FF527B'
              title='Abandoned carts'
              value={112}
              percent={4.2}
              classPercent='block-item__percent_positive'
              gridCol='col-1'
              classModificator='block-item_abandoned-carts'
            />
            <BarGraphic
              data={dataTotalVisitors}
              title='Total visitors'
              value={9124}
              gridCol='col-2'
              classModificator='block-item_total-visitors'
            />
            <HorizontalGraphic
              data={dataTraffic}
              title='Traffic Source'
              gridCol='col-1'
              classModificator='block-item_traffic-source'
            />
            <LatestOrders
              dataOrders={dataOrders}
              title='Latest orders'
              gridCol='full-width'
              classModificator='block-item_latest-orders'
            />
          </div>
        </div>
        <div className="block-item currency-wrapper">
          <Filter />
          <div className="currency-block">
            <div className="block-item__header">
              <h3 className="block-item__title">Fiat</h3>
              <a className="edit" href="#">Edit</a>
            </div>
            <CurrencyList data={dataCurrency} />
          </div>
          <div className="crypto-block">
            <div className="block-item__header">
              <h3 className="block-item__title">Crypto</h3>
              <a className="edit" href="#">Edit</a>
            </div>
            <CurrencyList data={dataCrypto} />
          </div>
        </div>
      </div>
    </>
  )
}

export default HomePage;