import Header from "../components/Header";

function OrdersPage({titlePage}) {
  return (
    <>
      <Header titlePage={titlePage} />
      <div className="wrapper-orders-content">
        content page Orders
      </div>
    </>
  )
}

export default OrdersPage;