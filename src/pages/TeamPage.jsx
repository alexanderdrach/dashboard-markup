import Header from "../components/Header";

function TeamPage({titlePage}) {
  return (
    <>
      <Header titlePage={titlePage} />
      <div className="wrapper-team-content">
        content page Team
      </div>
    </>
  )
}

export default TeamPage;