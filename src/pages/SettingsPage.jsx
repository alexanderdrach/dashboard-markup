import Header from "../components/Header";

function SettingsPage({ titlePage }) {
  return (
    <>
      <Header titlePage={titlePage} />
      <div className="wrapper-settings-content">
        content page Settings
      </div>
    </>
  )
}

export default SettingsPage;