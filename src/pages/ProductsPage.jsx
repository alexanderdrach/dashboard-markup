import Header from "../components/Header";

function ProductsPage({ titlePage }) {
  return (
    <>
      <Header titlePage={titlePage} />
      <div className="wrapper-products-content">
        content page Products
      </div>
    </>
  )
}

export default ProductsPage;